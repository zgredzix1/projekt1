#ifndef CDFILM_H
#define CDFILM_H
#include "item.h"

class CDFilm : public Item {
 public:
  CDFilm(std::string const& name, double price);
};

#endif
