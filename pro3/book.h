#ifndef BOOK_H
#define BOOK_H
#include "item.h"

class Book : public Item {
 public:
  Book(std::string const& name, double price);
};

#endif
