#ifndef KLIENT_H
#define KLIENT_H
#include <iostream>
#include <string>
#include "newspaper.h"
#include "item.h"
#include "book.h"
#include "list.h"
#include "cdfilm.h"
#include "cdgame.h"
#include "cdmusic.h"

class Client {
 private:


  double m_money_sum=0;
  int m_points=0;
  double m_money_left=100;
  std::string m_name;

  bool m_next_free;

 public:
    Client(std::string const& name);
    List<Item> m_bought;

    void Buy(Item const& item);
    void SelectPrize(Client*k);
    void GetBack();
    void PrintBought() const;
    std::string name() const;
    void SetBudget(double budget);
    int Getpoints();
    double Getmoney();
    double Getmoneyleft();
};

#endif
