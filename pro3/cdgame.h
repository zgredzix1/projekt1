#ifndef CDGAMEH
#define CDGAMEH
#include "item.h"

class CDGame : public Item {
 public:
  CDGame(std::string const& name, double price);
};

#endif
