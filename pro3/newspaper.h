#ifndef NEWSPAPER_H
#define NEWSPAPER_H
#include "item.h"

class Newspaper : public Item {
 public:
  Newspaper(std::string const& name, double price);
};

#endif
