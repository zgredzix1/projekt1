#include "item.h"
#include <iostream>
using namespace std;

Item::Item(double price, string const& name, int points)
    : m_price(price), m_name(name), m_points(points) {}

double Item::price() const
{ return m_price; }

string Item::name() const
 { return m_name; }

int Item::points() const
 { return m_points; }

ostream& operator<<(ostream& os, const Item& item) {
  cout << "Name: " << item.name() << ", Price: " << item.price()
            << ", Points: " << item.points();
  return os;
}
