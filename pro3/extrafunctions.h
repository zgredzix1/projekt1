#ifndef EXTRAFUNCTIONS_H
#define EXTRAFUNCTIONS_H
#include <iostream>
#include <cstdlib>
#include "client.h"


void MainMenu();
void MenuItems(Client*clientptr);
bool isRight();
bool isRight2();
void ItemChose(Client*clientptr);

template <typename T>
void CheckIfPossible(Client*clientptr,T l){
    if(clientptr->Getmoneyleft()<8)
        {
            system("cls");
            std::cout<<"There is no money left.You have: "<<clientptr->Getpoints()<<"points"<<std::endl<<std::endl
            <<"If possible select your prize:"<<std::endl;
            clientptr->SelectPrize(clientptr);
            std::cout<<"History of shopping:"<<std::endl;
            clientptr->PrintBought();
            exit(1);
        }
}

#endif
