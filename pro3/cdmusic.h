#ifndef CDMUSIC_H
#define CDMUSIC_H
#include "item.h"

class CDMusic : public Item {
 public:
  CDMusic(std::string const& name, double price);
};

#endif
