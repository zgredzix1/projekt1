#include "client.h"
#include <iostream>
using namespace std;

Client::Client(string const& name) : m_name(name) {}

void Client::Buy(Item const& item) {

        m_bought.add(item);
        m_money_sum += item.price();
        m_points += item.points();
        m_money_left-=item.price();
    if (m_next_free)
        m_next_free = false;
}
void Client::GetBack(){
    m_bought.clear();
    m_money_left+=m_money_sum;
    m_money_sum=0;
}
void Client::PrintBought() const {
    for ( unsigned int i = 0; i < m_bought.length(); i++) {
            cout <<"\t\t\t\t\t\t\t"<<i+1<<": "<<m_bought[i] << '\n';
        }
    if(m_bought.empty())
            cout<<"\t\t\t\t\t\t\t\t\t\tEmpty"<<endl;
}


string Client::name() const { return m_name; }

void Client::SelectPrize(Client *clientptr) {
        cout<<"Actual number of points:"<<clientptr->Getpoints()<<endl
            << "Choose a prize:\n"
            << "1: Free newspaper CD ACTION: 40 punktow\n"
            << "2: Free Music CD JOHN LENNON BEST HITS: 60 punktow\n "
            << "3: Free film SHREK: 80 punktow\n"
            << "Choice: ";

  int wybor;
  cin >> wybor;

  switch (wybor) {
    case 1:
      if (m_points >= 40) {
        cout
            << "You have chosen a newspaper !\n";
        m_next_free = true;
        Buy(Newspaper("CD ACTION", 10.99));
        m_money_sum -=10.99;
        m_points -= 40;
        m_money_left+=10.99;
        return;
      }
      break;
    case 2:
      if (m_points >= 60) {
        cout << "You have chosen a music CD!\n";
        m_next_free = true;
        Buy(CDMusic("JOHN LENNON BEST HITS", 42.99));
        m_money_sum -=42.99;
        m_points -= 60;
        m_money_left+=42.99;
        return;
      }
      break;
    case 3:
      if (m_points >= 80) {
        cout << "You have chosen a film!\n";
        m_next_free = true;
        Buy(CDFilm("SHREK", 56.99));
        m_money_sum -=56.99;
        m_points -= 80;
        m_money_left+=56.99;
        return;
      }
      break;
    default:
      cout << "Unknown\n";
      return;
      break;
  }

  cout << "Too little points!\n";
}
void Client::SetBudget(double budget){
    m_money_left=budget;
}
int Client::Getpoints(){
    return m_points;
}
double Client::Getmoney(){
    return m_money_sum;
}
double Client::Getmoneyleft(){
    return m_money_left;
}
