#ifndef ITEM_H
#define ITEM_H
#include <iostream>
#include <string>

class Item {
 public:
    Item(double price, std::string const& name, int points);

    double price() const;
    std::string name() const;
    int points() const;

    friend std::ostream& operator<<(std::ostream& os, const Item& item);

 protected:
    double m_price;
    std::string m_name;
    int m_points;
};

#endif
