#ifndef LIST_H
#define LIST_H
#include <iostream>


template <typename T>
class List {
 public:

  List(){};  //konstruktor domyslny
  List(unsigned size) { resize(size); }  // konstruktor tworzacy `size` element�w
  ~List() { clear(); }  // Destruktor, dealokujacy elementy

  List(List<T> const& other) { *this += other; }  // Konstruktor kopiujacy
  List<T>& operator=(List<T> const& other) {   // Operator kopiowania

    clear();
    // Dodaj druga liste do tej
    *this += other;
  }

  // Czyszczenie zawartosci listy
  void clear() {

    if (empty()) return;

    Element* actual_element = m_first; //aktualny element ustawiony na glowe listy
    Element* next_element = actual_element->next; //kolejny element// Dop�ki nastepny element istnieje

    while (next_element != 0) { // Dop�ki nastepny element istnieje

        delete actual_element;

        actual_element = next_element;
        next_element = next_element->next;

    }

        delete actual_element;

        m_length = 0;
        m_first =0;
        m_last = 0;
  }

  // Rozszerzanie wielkosci listy
  void resize(unsigned new_size) {

    if (length() >= new_size) return;

    while (length() < new_size) {
        add(T{});
        }
    }

  // Dodawanie jednego elementu do listy
  void add(T const& e) {

    Element* new_element = new Element(e);

    if (empty()) {
      // Dodaj go jako pierwszy i ostatni
        m_first = new_element;
        m_last = m_first;
    } else {

        m_last->next = new_element; // Jesli nie, to zlinkuj go z ostatnim
        m_last = new_element; // I ustaw go jako ostatni element na liscie
    }

        m_length++;
  }

  // Usuniecie ostatniego elementu z listy
  T del() {
    if (empty()) return T{};

    Element* e = m_first;

    for (unsigned i = 1; i < m_length - 1; i++) {   // Znajdz przedostatni element
        e = e->next;
    }

    // Zapisz wartosc ostatniego elementu, zeby ja zwrocic
    T value = m_last->data;

    // Usun ostatni element i zastap go przedostatnim
    delete m_last;
    e->next = 0;
    m_last = e;

    m_length--;

    return value;
  }

  unsigned length() const { return m_length; }  // Zwr�cenie wielkosci listy
  bool empty() const { return m_first == 0; } // Sprawdzenie czy lista jest pusta

  T& operator[](unsigned n) {
    if (n >= m_length) {
        std::cout<<"Wykroczenie poza zakres listy!";
        }

    Element* e = m_first;
    for (unsigned i = 0; i < n; i++) {
        e = e->next;
    }

        return e->data;
    }

  T const& operator[](unsigned n) const {
    if (n >= m_length) {
        std::cout<<"Wykroczenie poza zakres listy!";
    }

    Element* e = m_first;
    for (unsigned i = 0; i < n; i++) {
        e = e->next;
    }

        return e->data;
  }

  List<T>& operator+=(List<T> const& other) {
    Element* e = other.m_first;

    for (unsigned i = 0; i < other.length(); i++) {
        this->add(e->data);
        e = e->next;
    }
        return *this;
  }

  friend List<T> operator+(List<T> left, List<T> const& right) {
    left += right;
    return left;
  }

  friend std::ostream& operator<<(std::ostream& os, List<T> const& list) {
    Element* el = list.m_first;
    while (el != 0) {
        os << el->data << ' ';
        el = el->next;
    }

    return os;
  }

 private:
  // reprezentacja grupowa, klasa elemnt reprezentuje element na liscie
  class Element {
    public:
        Element* next=0;
        T data{};
        Element(T const& element_data) : data{element_data} {} // Konstruktor, zeby latwo mozna bylo tworzyc kolejne elementy
  };

  unsigned m_length=0;
  Element* m_first=0;
  Element* m_last=0;
};

#endif

