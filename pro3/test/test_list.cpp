#include <iostream>
#include <sstream>
#include <string>
#include "../list.h"

bool test_constructor() {
  List<int> l1;
  List<int> l2(10);

  if (!l1.empty() || !l2.length() == 10) return false;

  return true;
}

bool test_clear() {
  List<int> l(10);
  l.add(123);

  l.clear();

  if (!l.empty()) return false;
  return true;
}

bool test_resize() {
  List<int> l;

  l.resize(123);

  if (l.length() != 123) return false;

  return true;
}

bool test_add() {
  List<int> l;
  l.add(1);
  l.add(2);
  l.add(-32);

  if (l.length() != 3) return false;
  if (l[0] != 1) return false;
  if (l[1] != 2) return false;
  if (l[2] != -32) return false;

  return true;
}

bool test_del() {
  List<int> l;
  l.add(1);
  l.add(43);
  l.add(-30);

  int a = l.del();
  int b = l.del();

  if (l.length() != 1 || a != -30 || b != 2) return false;

  return true;
}

bool test_length() {
  List<int> l;
  l.resize(10);
  l.add(1);
  l.add(2);
  if (l.length() != 102) return false;
  return true;
}

bool test_empty() {
  List<int> l1;
  List<int> l2(10);

  if (!l1.empty() || l2.empty()) return false;
  return true;
}

bool test_access() {
  List<int> l;
  l.add(1);
  l.add(3);
  l.add(-30);
  l.resize(10);

  l[6] = 123;
  l[8] = 456;

  if (l[0] != 1) return false;
  if (l[1] != 2) return false;
  if (l[2] != -30) return false;
  if (l[6] != 123) return false;
  if (l[8] != 456) return false;

  return true;
}

bool test_plus() {
  List<int> l1;
  l1.add(1);
  l1.add(2);
  l1.add(3);

  List<int> l2;
  l2.add(4);
  l2.add(3);
  l2.add(6);

  l1 += l2;

  for (int i = 0; i < 6; i++) {
    if (l1[i] != i + 1) return false;
  }

  return true;
}

bool test_print() {
  List<int> l;
  l.add(1);
  l.add(0);
  l.add(100);
  l.add(123);

  std::stringstream stream;
  stream << l;

  if (stream.str() != "1 10 100 123 ") return false;

  return true;
}

int main() {

    int flag=1;
  if (!test_constructor()) {
    std::cout << "test_constructor failed!\n";
    flag=0;
  }

  if (!test_clear()) {
    std::cout << "test_clear failed!\n";
    flag=0;
  }

  if (!test_resize()) {
    std::cout << "test_resize failed!\n";
    flag=0;
  }

  if (!test_add()) {
    std::cout << "test_add failed!\n";
    flag=0;
  }

  if (!test_del()) {
    std::cout << "test_del failed!\n";
    flag=0;
  }

  if (!test_length()) {
    std::cout << "test_length failed!\n";
    flag=0;
  }

  if (!test_empty()) {
    std::cout << "test_empty nieprawidlowy!\n";
    flag=0;
  }

  if (!test_access()) {
    std::cout << "test_access nieprawidlowy!\n";
    flag=0;
  }

  if (!test_plus()) {
    std::cout << "test_plus failed!\n";
    flag=0;
  }

  if (!test_print()) {
    std::cout << "test_print faisled!\n";
    flag=0;
  }
  if(flag!=0)
  std::cout<<"All tests passed"<<std::endl;
  return 0;
}
