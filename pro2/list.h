#ifndef LIST_H
#define LIST_H
#include <iostream>
#include "complex.h"
using namespace std;

class List{
private:
    int amount;

public:
    Complex*first;

    List(){
    first=NULL;
    amount=0;
    };

    ~List() {
    Complex* temp = first;

        while (temp) {

            first=temp->next;
            delete temp;
            temp = first;

	}
	}
    void add(float real,float imag);
    int searchfor(float real,float imag);
    void del();

friend ostream & operator<< (ostream &out,const List&lista);

Complex&operator[](int i);

};
inline List operator+ (const List&a,const List&b)
{
   Complex *s=a.first;

        while (s->next)
        {
            s = s->next;         // znajdujemy wskaznik na ostatni element
        }

        s->next = b.first;
        return a;
}

#endif
