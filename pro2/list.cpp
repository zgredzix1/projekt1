#include <iostream>
#include "list.h"
#include "complex.h"
using namespace std;

void List::add(float real,float imag)  //funkcja dodajaca element do listy
{
    Complex *newcomplex = new Complex;    // tworzy nowy element listy

    newcomplex->real = real; // wypełniamy naszymi danymi
    newcomplex->imag = imag;

    if (first==0) // sprawdzamy czy to pierwszy element listy
    {
       first = newcomplex;  // jeżeli tak to nowy element jest teraz początkiem listy
    }

    else
    {
        Complex *s = first; // w przeciwnym wypadku wędrujemy na koniec listy

        while (s->next)
        {
            s = s->next;         // znajdujemy wskaźnik na ostatni element
        }

        s->next = newcomplex;  // ostatni element wskazuje na nasz nowy
        newcomplex->next= 0;     // ostatni nie wskazuje na nic
    }
    ++amount;
}

int List::searchfor(float realistic,float imaginary) //metoda wyszukujaca
{                                                     //po wskazanej czesci rzeczywistej i urojonej
Complex *s=first;

while (s != NULL) {
   if (s->real==realistic && s->imag==imaginary) {
      cout<<*s;
      return 1;
      }
         s = s->next; //przechodzimy do kolejnego elementu listy
         }
        return 0;
}
void List::del(){   //metoda usuwajaca ostatni element z listy
  Complex * p = first;
  if(p)
  {
    if(p->next)
    {
      while(p->next->next) p = p->next;
      delete p->next;
      p->next = NULL;
    }
    else
    {
      delete p;    // lista jednoelementowa
      first = NULL; // staje się listą pustą
    }
  }
}

ostream & operator<< (ostream &out,const List&lista)
{
    Complex *p=lista.first;
    if(p==0){
        cout<<"Empty list"<<endl;

    }
    while(p!=0){
        out << p -> real<<"+"<<p->imag<<"j"<<endl;
        p=p->next;
    }
    return out;
}

Complex& List::operator[](int i){

    Complex*s=first;
    if(s==0 || i>amount){
    cout<<"error"<<endl;
    }
    int k=0;
        while(k!=i)
        {
            ++k;
            s=s->next;
        }
    return *s;
}


