
#include <iostream>
#include "functions.h"
#include "list.h"
#include "complex.h"
using namespace std;


void menu() //menu kontekstowe nr1
{
cout<<endl;
cout<<"  \t\t\t         MENU"<<endl;
cout<<"\t\t\t1: ADD A COMPLEX NUMBER"<<endl;
cout<<"\t\t\t2: SHOW LIST USING OPERATOR <<"<<endl;
cout<<"\t\t\t3: SEARCH WITH REAL AND IMAGINARY PART"<<endl;
cout<<"\t\t\t4: DELETE LAST ELEMENT"<<endl;
cout<<"\t\t\t5: OPERATIONS +,+=,-,-=,==,<< ON COMPLEX NUMBERS"<<endl;
cout<<"\t\t\t6: OPERATIONS <<,+,[] ON LISTS"<<endl;
cout<<"\t\t\t7: QUIT"<<endl;
}
void menu2() //menu kontekstowe nr2
{
cout<<endl;
cout<<"\t\t\t           MENU2"<<endl;
cout<<"\t\t\t1: +"<<endl;
cout<<"\t\t\t2: +="<<endl;
cout<<"\t\t\t3: -"<<endl;
cout<<"\t\t\t4: -="<<endl;
cout<<"\t\t\t5: =="<<endl;
cout<<"\t\t\t6: <<"<<endl;
cout<<"\t\t\t7:BACK TO MAIN MENU"<<endl;
}
void menu3() //menu kontekstowe nr3
{
cout<<endl;
cout<<"\t\t\t1: Add complex numbers to list 1"<<endl;
cout<<"\t\t\t2: Show list 1"<<endl;
cout<<"\t\t\t3: Add complex numbers to list 2"<<endl;
cout<<"\t\t\t4: Show list 2"<<endl;
cout<<"\t\t\t5: Merge two lists"<<endl;
cout<<"\t\t\t6: Show element of list 1 using [] operator"<<endl;
cout<<"\t\t\t7:BACK TO MAIN MENU"<<endl;

}

void fun2 (int choice, Complex z1,Complex z2,Complex z3){

do{
        menu2();
        cin>>choice;

            switch(choice){
        case 1:
            cout<<"Solution of z1+z2 is: "<<endl;
            z3=z1+z2;
            cout<<z3;
            break;
        case 2:
            cout<<"Solution of z1+=z2 is: "<<endl;
            z3=z1+=z2;
            cout<<z3;
            break;
        case 3:
            cout<<"Solution of z1-z2 is: "<<endl;
            z3=z1-z2;
            cout<<z3;
            break;
        case 4:
            cout<<"Solution of z1-=z2 is: "<<endl;
            z3=z1-=z2;
            cout<<z3;
            break;
        case 5:
            if(z1==z2)
                cout<<"Equal"<<endl;
            else
                cout<<"Not equal"<<endl;
            break;
        case 6:
            cout<<"Z1="<<z1<<endl;
            cout<<"Z2="<<z2<<endl;
            break;
        default:
            cout<<"unknown"<<endl;
            break;
            }
        }while(choice!=7);

}
void fun3(int choice,List base2,List base3,List base4,double real,double imag)
{
    int index;

        do{
        menu3();
        cin>>choice;

            switch(choice){
        case 1:
            cout<<"Type a real and after the imag part of complex number"<<endl;
            cin>>real>>imag;

            base2.add(real,imag);
        break;
        case 2:
            cout<<"LIST 1:"<<endl;
            cout<<base2;
            break;
        case 3:
            cout<<"Type a real and after the imag part of complex number"<<endl;
            cin>>real>>imag;

            base3.add(real,imag);
            break;
        case 4:
            cout<<"LIST 2:"<<endl;
            cout<<base3;
            break;
        case 5:
            cout<<"Merged list1 and list2"<<endl;
            base4=base2+base3;
            cout<<base4;
            break;
        case 6:
            cout<<"Give an [] index you want to show: ";
            cin>>index;
            cout<<base2[index];
            break;
        default:
            cout<<"unknown"<<endl;
            break;
            }
        }while(choice!=7);
}
