#ifndef CLASS_H
#define CLASS_H
#include <iostream>
using namespace std;


class Complex{   //klasa Liczba Zespolona
public:
    float real;
    float imag;

Complex *next=0;   //wskaznik umozliwiajacy przejscie do kolejnego elementu

Complex(double r=0,double i=0)   //konstruktor
{
    real=r;
    imag=i;
}
~Complex(){};    //destruktor

Complex operator +(Complex A);    //kolejno zdefiniowane przeciazenia operatorow
Complex operator +=(Complex B);
Complex operator -=(Complex C);
Complex operator -(Complex D);
bool operator ==( Complex F );

friend ostream & operator<< (ostream &wyjscie,Complex &E); //operator wyjscia <<

};


#endif

