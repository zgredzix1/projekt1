//IGOR RUDNICKI 2AR
//PROJEKT NR 1
#include <iostream>
#include "functions.h"
#define NUMB 10
using namespace std;

int main()
{
    cout<<"This program calculates the roots of the quadratic function.Press enter to start"<<endl;
    int i=0;
    Fun f1[NUMB];
    Fun* f2=new Fun; //przykladowy wskaznik by pokazac wyciek pamieci
    while(i<NUMB )
    {
    f1[i].give(); //metoda proszaca o podanie wspolczynnikow kolejnych funkcji(obiektow)
    	if(f1[i].check()!=1) //metoda sprawdzajaca warunek czy a nie jest rowne zero
    {
       cout<<"Error with initial data"<<endl;
       continue;               //jesli a jest rowne zero to ten obieg petli pomijamy
    }
    f1[i].countplaces();  //metoda liczaca miejsca zerowe
    ++i;
    }

    delete f2;
    return 0;
}
