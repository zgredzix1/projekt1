#pragma once
#ifndef _GAME_SCREEN_H
#define _GAME_SCREEN_H

#include "Screen.h"
#include "ScreenManager.h"
#include "ScoresScreen.h"
#include <queue>

using namespace sf;

class SnakeGame;
class Rectangle;

class GameScreen : public DefaultScreen //klasa GameScreen dziedziczaca po klasie Screen
{
public:

	const static std::string ID;

	GameScreen(SnakeGame *game, ScreenManager *manager);

	void Start() override; //funkcje wirtualne w trybie override(nadpisania)
	void Update() override;
	void InputUpdate(Event &e) override;
	void Draw(RenderWindow &window) override;

	Font font;       //obiekt klasy sf::Font
	Text scoreText1;
	Text scoreText2;  //teksty do wypisania na ekran, obiekty klasy sf::Text
	Text scoreText3;
	Text gameOverText;
	Text highScoreText;
	Text winText;
	Texture texture;
    Sprite sprite;
    std::vector<int> highscores;


private:
	SnakeGame* game;
	ScreenManager* manager;

	static const int WIDTH = 32;
	static const int HEIGHT = 32;

	int boardArr[HEIGHT][WIDTH];
	int rectWidth = 20;
	Rectangle* rectArr[32][32];
	Vector2<int> speed;
	std::vector<Vector2<int>> snakeList;
	std::queue<Vector2<int>> digestingAppleQ;

	Vector2<int> startPos;
	Vector2<int> applePos;

    int UpdateQuit(Event &e);
	void createBoard(); //funkcja tworzaca tablice obiektow tile(prostokacikow)
	bool spawnApple();  //funkcja losujaca wspolrzedna i spawnujaca w tym miejscu jalbko
	void setSpeed(int x, int y); //funkcja ustawiajaca kierunek przemieszczania sie snake'a
	bool isCollideSelf();  //funkcja sprawdzajac czy waz nie zjadl siebie
	void checkCollideApple();  //funkcja sprawdzajaca czy nie doszlo do zetkniecia z jablkiem
	void appendSnakeBody(int x, int y);
	void digestApple();  //funkcja powodujaca zjedzenie jablka
	bool moveSnake();  //funkja przemieszcajaca cialo snake'a i sprawdzajaca czy nie uderzyl w sciane
	bool updateScore(); //funkcja ktora sluzy do update'u wyniku
	void gameOver(); //funkcja uzyta w przypadku konca gry
	bool running;     //zmienna boolowska okreslajaca czy gra jest w trybie run


	int score;     //zmienna przechowujaca aktualny wynik
};

#endif // !_GAME_SCREEN_H
