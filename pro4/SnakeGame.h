#pragma once

#ifndef SNAKE_GAME_H
#define SNAKE_GAME_H

#include <iostream>
#include "SFML/Graphics.hpp"
#include "Rect.h"
#include "ScreenManager.h"


class MainMenuScreen;
class GameScreen;
class OptionsScreen;
class ScoresScreen;

using namespace sf;

class SnakeGame {
public:
	SnakeGame();

	void Init();
	void Update();
	void UpdateInput();
	void Draw();

	bool running = true;
	int pace;
	void SetPace(const int &p);
	Font font;

GameScreen *gameScreen;
private:


	RenderWindow *window;

	MainMenuScreen *mainMenuScreen;
	OptionsScreen *optionsScreen;
	ScoresScreen *scoresScreen;

	ScreenManager screenManager;


};

#endif


