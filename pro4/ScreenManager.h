#pragma once
#include <iostream>
#include <map>

class DefaultScreen;

class ScreenManager
{
public:
	ScreenManager();
	~ScreenManager();

	DefaultScreen* currentScreen;
	std::map<std::string, DefaultScreen*> Screens;

	void switchScene(std::string sceneId);

};

