#include "SnakeGame.h"
#include "SFML/Audio.hpp"

int main()
{
    SnakeGame *snakeGame = new SnakeGame(); //nowy obiekt klasy SnakeGame
    snakeGame->SetPace(100);
    sf::Music music;
    try{                                     //obsluga wyjatku z proba wczytania muzyki z pliku
    if(!music.openFromFile("snake.ogg"))
       throw "Cannot load from music file\n";
    }
    catch(const char*exceptm){
    std::cout<<"Exception occured "
    <<exceptm;
    }
	music.play();
	music.setVolume(30.f);
	snakeGame->Init();

	delete snakeGame; //usuniecie obiektu snakeGame
	return 0;
}

