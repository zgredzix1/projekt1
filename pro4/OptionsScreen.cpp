#include <SFML/Graphics.hpp>
#include "MainMenuScreen.h"
#include "OptionsScreen.h"
#include "ariblkfont.h"
#include "SnakeGame.h"
#include "GameScreen.h"
const int pace1=50;
const int pace2=100;
const int pace3=150;

const std::string OptionsScreen::ID = "OptionsScreen";

OptionsScreen::OptionsScreen(SnakeGame *game, ScreenManager* manager)
{
	this->game = game;
	this->manager = manager;

    try{
	if(!texture.loadFromFile("optionsbg.jpg"))
        throw "Loading texture failed";
    sprite.setTexture(texture);
	}catch(const char*exceptfont)
	{
	    std::cout<<"Exception occured"<<exceptfont;
	}

    try{

	if (!font.loadFromMemory(&ariblkfont, ariblkfont_len))
		throw "Loading font failed";
    }
    catch(const char*exceptfont)
    {
        std::cout<<"Exception occured "<<exceptfont;
    }


		textFront.setFont(font);
		textFront.setString("OPTIONS");
		textFront.setCharacterSize(120);
		textFront.setFillColor(Color::White);
		textFront.setPosition(60, 80);

		textOptions.setFont(font);
		textOptions.setString("PRESS :\n1- HIGH SPEED\n2- NORMAL SPEED\n3- LOW SPEED");
		textOptions.setCharacterSize(30);
		textOptions.setFillColor(Color::White);
		textOptions.setPosition(50, 250);

		textQuit.setFont(font);
		textQuit.setString("Q-quit");
		textQuit.setCharacterSize(50);
		textQuit.setFillColor(Color::White);
		textQuit.setPosition(450, 500);

}


void OptionsScreen::Start()
{

}

void OptionsScreen::Update()
{

}

void OptionsScreen::InputUpdate(Event &e)
{

	if (e.type == Event::KeyPressed) {
		if (e.key.code == Keyboard::Key::Q) {
			manager->switchScene(MainMenuScreen::ID);
		}
		if (e.key.code == Keyboard::Key::Num1) {
			game->SetPace(pace1);
			manager->switchScene(MainMenuScreen::ID);
		}
		if (e.key.code == Keyboard::Key::Num2) {
			game->SetPace(pace2);
			manager->switchScene(MainMenuScreen::ID);
		}
		if (e.key.code == Keyboard::Key::Num3) {
			game->SetPace(pace3);
			manager->switchScene(MainMenuScreen::ID);
		}
	}
}

void OptionsScreen::Draw(RenderWindow& window)
{
    window.draw(sprite);
	window.draw(textFront);
	window.draw(textOptions);
	window.draw(textQuit);

}

