#include "Rect.h"



Rectangle::Rectangle()
{

	rect = new RectangleShape();
	rect->setFillColor(Color::Black);
	rect->setSize(size);
	rect->setPosition(Vector2<float>(position.x * size.x, position.y * size.y));

	isActive = true;
}

Rectangle::~Rectangle()
{
	delete(rect);
}

void Rectangle::draw(RenderWindow* renderer)
{
	if (isActive)
	{
		rect->setFillColor(activeColor);
	}
	else
	{
		rect->setFillColor(Color::Black);
	}
	renderer->draw(*this->rect);
}

void Rectangle::setPosition(float x, float y)
{
	position.x = x;
	position.y = y;

	rect->setPosition(x*size.x, y*size.y);
}

void Rectangle::setWidth(int width)
{
	size.x = width;
	size.y = width;

	rect->setSize(size);
}

Vector2<float>* Rectangle::getPosition()
{
	return &position;
}

void Rectangle::setActiveColor(Color color) {
	this->activeColor = color;
}
