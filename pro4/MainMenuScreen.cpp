#include <SFML/Graphics.hpp>
#include "MainMenuScreen.h"
#include "OptionsScreen.h"
#include "ScoresScreen.h"
#include "ariblkfont.h"
#include "SnakeGame.h"
#include "GameScreen.h"


const std::string MainMenuScreen::ID = "MainMenuScreen";

MainMenuScreen::MainMenuScreen(SnakeGame *game, ScreenManager* manager)
{
	this->game = game;
	this->manager = manager;

    try{

    if(!texture.loadFromFile("snw.jpg"))
        throw "Loading texture failed";
    sprite.setTexture(texture);
    }
    catch(const char*exceptfont)
    {
        std::cout<<"Exception occured"<<exceptfont;
    }

    try{
	if (!font.loadFromMemory(&ariblkfont, ariblkfont_len))
		throw "Loading font failed";
    }
    catch(const char*exceptfont)
    {
        std::cout<<"Exception occured"<<exceptfont;
    }


		textTitle.setFont(font);
		textTitle.setStyle(sf::Text::Italic);
		textTitle.setString("SNAKE\nGAME");
		textTitle.setCharacterSize(80);
		textTitle.setFillColor(Color::Green);
		textTitle.setPosition(10, 5);

		textHelp.setFont(font);
		textHelp.setString("PRESS:\nL TO START\nQ TO QUIT\nO GO TO OPTIONS\nH TO GO TO SCORES");
		textHelp.setCharacterSize(25);
		textHelp.setFillColor(Color::White);
		textHelp.setPosition(350, 220);

		textGoal.setFont(font);
		textGoal.setString("SCORE 40 POINTS\nAND WIN THE GAME!");
		textGoal.setCharacterSize(36);
		textGoal.setFillColor(Color::Green);
		textGoal.setPosition(15, 400);

}


void MainMenuScreen::Start()
{

}

void MainMenuScreen::Update()
{

}

void MainMenuScreen::InputUpdate(Event &e)
{

	if (e.type == Event::KeyPressed) {
		if (e.key.code == Keyboard::Key::L) {
			manager->switchScene(GameScreen::ID);
		}
		if (e.key.code == Keyboard::Key::O) {
			manager->switchScene(OptionsScreen::ID);
		}
		if (e.key.code == Keyboard::Key::H) {
			manager->switchScene(ScoresScreen::ID);
		}
		if (e.key.code == Keyboard::Key::Q) {
			exit(1);
		}
	}
}

void MainMenuScreen::Draw(RenderWindow& window)
{
    window.draw(sprite);
	window.draw(textTitle);
	window.draw(textHelp);
	window.draw(textGoal);

}

