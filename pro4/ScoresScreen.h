#pragma once
#ifndef _SCORES_SCREEN_H
#define _SCORES_SCREEN_H
#define MAX 10

#include "Screen.h"
#include "GameScreen.h"
#include "ScreenManager.h"
using namespace sf; //uzyte przy uzywaniu klas z biblioteki SFML

class SnakeGame;
class GameScreen;

class ScoresScreen : public DefaultScreen
{
public:

	const static std::string ID;

	ScoresScreen(SnakeGame *game, ScreenManager *manager,GameScreen *games);

	void Start() override;      //funkcje wirtualne w trybie override(nadpisywania)
	void Update() override;
	void InputUpdate(Event &e) override;
	void Draw(RenderWindow &window) override;

	Font font; //obiekt klasy sf::Font

	Text textFront;  //teksty do wypisania w MainMenu, obiekty klasy sf::Text
	Text textQuit;
	Text textScores;
	Text Score[MAX];


	Texture texture;
    Sprite sprite;

private:
	SnakeGame* game;
	ScreenManager* manager;
	GameScreen* games;
};

#endif
