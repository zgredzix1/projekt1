#pragma once
#ifndef _MAIN_MENU_SCREEN_H
#define _MAIN_MENU_SCREEN_H

#include "Screen.h"
#include "ScreenManager.h"
using namespace sf; //uzyte przy uzywaniu klas z biblioteki SFML

class SnakeGame;

class MainMenuScreen : public DefaultScreen  //klasa MainMenuScreen dziedziczaca po klasie Screen
{
public:

	const static std::string ID;

	void Start() override;                        //funkcje wirtualne w trybie override(nadpisywania)
	void Update() override;
	void InputUpdate(Event &e) override;
	void Draw(RenderWindow &window) override;

	MainMenuScreen(SnakeGame *game, ScreenManager *manager);

	Font font; //obiekt klasy sf::Font

	Text textTitle;  //teksty do wypisania w MainMenu, obiekty klasy sf::Text
	Text textHelp;
	Text textGoal;
    Texture texture;
    Sprite sprite;

private:
	SnakeGame* game;
	ScreenManager* manager;
};

#endif // !_MAIN_MENU_SCREEN_H
