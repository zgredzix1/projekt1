#pragma once
#include <SFML\Graphics.hpp>
#include <sstream>
#include <string>
using namespace sf;

class DefaultScreen  //tzw klasa abstrakcyjna, od ktorej dziedzicza inne klasy
{
public:
	virtual void Start() = 0;                     //funkcja wirtualna uzyta przy starcie danego 'screenu'
	virtual void Update() = 0;                    //funkcja aktualizujaca zmiany dla danego 'screenu'
	virtual void InputUpdate(Event &e) = 0;       //funkcja aktualizujaca zmiany wejscia( np input WASD) dla danego s'screenu'
	virtual void Draw(RenderWindow &window) = 0;    //funkcja wyrysowujaca dane obiekty w wyrenderowanym oknie danego 'screenu

	std::string to_string( int i ) { //wlasna funkcja to_string
    std::ostringstream o;
    o << i;
    return o.str();
    }

};

