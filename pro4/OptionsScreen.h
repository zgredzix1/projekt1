#pragma once
#ifndef _OPTIONS_SCREEN_H
#define _OPTIONS_SCREEN_H

#include "Screen.h"
#include "ScreenManager.h"
using namespace sf; //uzyte przy uzywaniu klas z biblioteki SFML

class SnakeGame;

class OptionsScreen : public DefaultScreen
{
public:

	const static std::string ID;

	OptionsScreen(SnakeGame *game, ScreenManager *manager);

	void Start() override;      //funkcje wirtualne w trybie override(nadpisywania)
	void Update() override;
	void InputUpdate(Event &e) override;
	void Draw(RenderWindow &window) override;

	Font font; //obiekt klasy sf::Font

	Text textFront;  //teksty do wypisania w MainMenu, obiekty klasy sf::Text
	Text textOptions;
	Text textQuit;
	Texture texture;
    Sprite sprite;


private:
	SnakeGame* game;
	ScreenManager* manager;
};

#endif

