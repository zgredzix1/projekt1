#include <SFML\Graphics.hpp>
#include "SnakeGame.h"
#include <iostream>
#include <random>
#include "ariblkfont.h"
#include "MainMenuScreen.h"
#include "OptionsScreen.h"
#include "ScoresScreen.h"
#include "GameScreen.h"


SnakeGame::SnakeGame()
{
	mainMenuScreen = new MainMenuScreen(this, &screenManager);
	gameScreen = new GameScreen(this, &screenManager);
	optionsScreen = new OptionsScreen(this, &screenManager);
	scoresScreen = new ScoresScreen(this, &screenManager,gameScreen);

	screenManager.Screens.insert(std::pair<std::string, DefaultScreen*>(MainMenuScreen::ID, dynamic_cast<DefaultScreen *>(mainMenuScreen)));
	screenManager.Screens.insert(std::pair<std::string, DefaultScreen*>(GameScreen::ID, dynamic_cast<DefaultScreen *>(gameScreen)));
	screenManager.Screens.insert(std::pair<std::string, DefaultScreen*>(OptionsScreen::ID, dynamic_cast<DefaultScreen *>(optionsScreen)));
    screenManager.Screens.insert(std::pair<std::string, DefaultScreen*>(ScoresScreen::ID, dynamic_cast<DefaultScreen *>(scoresScreen)));
}

void SnakeGame::Init()
{
	screenManager.switchScene(MainMenuScreen::ID);

	window = new RenderWindow(VideoMode(640, 640), "Snake Game");


	Clock clock;
	Time elapsed = clock.getElapsedTime();
	clock.restart();
    try{
	if (!font.loadFromMemory(&ariblkfont, ariblkfont_len))
        throw "Loading font failed";
    }
	catch(const char*except)
    {
        std::cout<<"Exception occured"
        <<except;
    }

	while (window->isOpen())
	{

		elapsed = clock.getElapsedTime();
		// tick
		if (elapsed.asMilliseconds() > pace && running)
		{
			clock.restart();
			Update();
		}

		Draw();
        UpdateInput();

	}
}

void SnakeGame::Update()
{
    try{
	if (screenManager.currentScreen != NULL)
        screenManager.currentScreen->Update();

	else if(screenManager.currentScreen== NULL)
        throw "Error with screenManager";
        }

    catch(const char*except)
        {
            std::cout<<"exception occured"
            <<except;
        }

}


void SnakeGame::Draw() {
	// drawing

	window->clear(Color::Red);

	if (screenManager.currentScreen != NULL)
	{
		screenManager.currentScreen->Draw(*window);
	}

	window->display();
}

void SnakeGame::UpdateInput()
{
	Event e;
	while (window->pollEvent(e))
	{
		if (e.type == Event::Closed)
		{
			window->close();
		}


		if (screenManager.currentScreen != NULL)
		{
			screenManager.currentScreen->InputUpdate(e);
		}


	}
}
void SnakeGame::SetPace(const int &p)
{
    try{
    pace=p;
    if(p<=0)
        throw "Pace must be greater than 0!";
    }
    catch(const char*except)
    {
        std::cout<<"Exception occured p"<<except;
    }
}
