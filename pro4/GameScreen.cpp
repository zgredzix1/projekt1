#include "GameScreen.h"
#include "SnakeGame.h"
#include <iostream>
#include "MainMenuScreen.h"

#include "ariblkfont.h"
#include <sstream>
#include <string>
#include <time.h>
#include <iostream>
const std::string GameScreen::ID = "GameScreen";


GameScreen::GameScreen(SnakeGame* game, ScreenManager* manager)
{
	this->game = game;
	this->manager = manager;
	this->score= score;


	speed.x = 1;
	speed.y = 0;

	startPos.x = 10;
	startPos.y = 10;

	createBoard();
    try{
	if (!font.loadFromMemory(&ariblkfont, ariblkfont_len))
		throw "Loading font failed";
    }catch(const char*exceptfont)
	{
	    std::cout<<"Exception occured"<<exceptfont;
	}

	    scoreText1.setFont(font);
	    scoreText1.setString("Actual score:");
        scoreText1.setCharacterSize(40);
		scoreText1.setFillColor(Color::White);
		scoreText1.setPosition(320, 20);

		scoreText2.setFont(font);
        scoreText2.setCharacterSize(40);
		scoreText2.setFillColor(Color::White);
		scoreText2.setPosition(580, 20);

		gameOverText.setFont(font);
		gameOverText.setStyle(sf::Text::Italic);
		gameOverText.setString("GAME OVER\nYOU SCORED");
		gameOverText.setCharacterSize(70);
		gameOverText.setFillColor(Color::White);
		gameOverText.setPosition(20,200);

		scoreText3.setFont(font);
        scoreText3.setCharacterSize(70);
		scoreText3.setFillColor(Color::White);
		scoreText3.setPosition(340, 350);


		highScoreText.setFont(font);
        highScoreText.setCharacterSize(30);
		highScoreText.setFillColor(Color::Black);
		highScoreText.setPosition(340, 350);

		winText.setFont(font);
		winText.setString("GOOD JOB YOU WON!");
		winText.setCharacterSize(90);
		winText.setFillColor(Color::Black);
		winText.setPosition(40, 150);


}



void GameScreen::Start()
{
	score = 0;
	running = true;

	snakeList.clear();

	speed.x = 1;
	speed.y = 0;

	while (!digestingAppleQ.empty())
	{
		digestingAppleQ.pop();
	}

	// create some body
	for (int i = 0; i < 3; i++)
	{
		appendSnakeBody(startPos.x - i, startPos.y);
	}

	int snakeBodyIt = 0;
	for (auto it = snakeList.begin(); it != snakeList.end(); ++it)
	{
		it->x = startPos.x - snakeBodyIt;
		it->y = startPos.y;

		snakeBodyIt++;
	}
	spawnApple();
}

void GameScreen::Update()
{
	if (!running )
		return;

	// move snake
	if (moveSnake()) {
		// check collision with head
		if (isCollideSelf() || score >=40) {
			running = false;
		}

		checkCollideApple();
		digestApple();

	}
	else {
		running = false;
	}

}

bool GameScreen::moveSnake()
{
	int prevX = 0, prevY = 0;
	for (std::vector<Vector2<int>>::iterator it = snakeList.begin(); it != snakeList.end(); ++it) {
		if (it == snakeList.begin()) {
			prevX = it->x;
			prevY = it->y;

			int nextX = prevX + speed.x;
			int nextY = prevY + speed.y;

			// move head
			if (nextX >= 0 && nextX <= WIDTH - 1) { //sprawdzenie czy waz nie uderzyl
				it->x = nextX;                  //w pionowa sciane
			}
			else {
				std::cout << "Snake hit wall!" << std::endl;
				gameOver();
				score=0;
                scoreText2.setString(to_string(score));

				return false;
			}
			if (nextY >= 0 && nextY <= HEIGHT - 1) { //sprawdzenie czy waz nie uderzyl
				it->y = nextY;                      //w pozioma sciane
			}
			else {
				std::cout << "Snake hit wall!" << std::endl;
				gameOver();
				score=0;
                scoreText2.setString(to_string(score));
				return false;
			}

		}
		else
		{
			int tmpX = it->x;
			int tmpY = it->y;

			it->x = prevX;
			it->y = prevY;

			prevX = tmpX;
			prevY = tmpY;

		}
	}
	return true;
}

void GameScreen::InputUpdate(Event &e)
{
	if (e.type == Event::KeyPressed) {
		if (e.key.code == Keyboard::Key::Up || e.key.code ==Keyboard::Key::W) {
			setSpeed(0, -1);
		}
		else if (e.key.code == Keyboard::Key::Left || e.key.code ==Keyboard::Key::A) {
			setSpeed(-1, 0);
		}
		else if (e.key.code == Keyboard::Key::Down || e.key.code ==Keyboard::Key::S) {
			setSpeed(0, 1);
		}
		else if (e.key.code == Keyboard::Key::Right || e.key.code ==Keyboard::Key::D) {
			setSpeed(1, 0);
		}
		else if (e.key.code == Keyboard::Key::Q) {
			manager->switchScene(MainMenuScreen::ID);
		}
		else if (e.key.code == Keyboard::Key::L) {
		    score=0;
            scoreText2.setString(to_string(score));
			manager->switchScene(MainMenuScreen::ID);
		}

	}
}

void GameScreen::Draw(RenderWindow &window)
{
	// clear board
	for (int i = 0; i < HEIGHT; i++) {
		for (int j = 0; j < WIDTH; j++) {
			//window.draw(*shapeArr[i][j]);
			Rectangle* rect = rectArr[i][j];

			rect->isActive = false;

			rect->draw(&window);
		}
	}

	// draw snake
	for (std::vector<Vector2<int>>::iterator it = snakeList.begin(); it != snakeList.end(); ++it) {
		Rectangle* rect = rectArr[it->y][it->x];
		rect->isActive = true;
		rect->setActiveColor(Color::White);
		rect->draw(&window);
	}

	// draw apple
	Rectangle* appleRect = rectArr[applePos.y][applePos.x];
	appleRect->isActive = true;
	appleRect->setActiveColor(Color::Red);
	appleRect->draw(&window);

	//draw score and texture
	window.draw(scoreText1);
	window.draw(scoreText2);

	if (!running)
    {

		window.draw(gameOverText);
		window.draw(scoreText3);
    }
}


void GameScreen::createBoard()
{
	for (int i = 0; i < HEIGHT; i++) {
		for (int j = 0; j < WIDTH; j++) {
			Rectangle *rect=new Rectangle();
			rect->setWidth(rectWidth);
			rect->setPosition(j, i);

			rectArr[i][j] = rect;
		}
	}
}

void GameScreen::setSpeed(int x, int y)
{

	if ((speed.x == 0 && x != 0 )||( speed.y == 0 && y != 0))
	{
		speed.x = x;
		speed.y = y;
	}

}


bool GameScreen::isCollideSelf()
{

	if (snakeList.empty())
        return 1;


	Vector2<int> head = snakeList.front();
	auto it = snakeList.begin() + 1;
	bool collide = false;
	while (it != snakeList.end() && !collide) {

		if (it->x == head.x && it->y == head.y) {
			std::cout << "Collision with body at " << it->x << "," << it->y << std::endl;
			collide = true;
			gameOver();
			score=0;
            scoreText2.setString(to_string(score));
		}
		else
			++it;
	}
	return collide;

}

void GameScreen::checkCollideApple()
{

	if (snakeList.empty())
		return;

	auto head = snakeList.begin() + 1;

	if (applePos.x == head->x && applePos.y == head->y)
	{
		Vector2<int> digestedApple(applePos.x, applePos.y);
		digestingAppleQ.push(digestedApple);
    try{

		if(!updateScore() || !spawnApple())
            throw "Score hasnt been updated or apple hasnt been spawned";
    }
    catch(const char*except)
    {
        std::cout<<"Exception occured"
        <<except;
    }

}
}

void GameScreen::appendSnakeBody(int x, int y)
{
	Vector2<int> body(x, y);
	snakeList.emplace_back(body);
}

bool GameScreen::spawnApple()
{
	int x = rand() % WIDTH;
	int y = rand() % HEIGHT;

	applePos.x = x;
	applePos.y = y;
	return true;
}

void GameScreen::digestApple()
{
	if (snakeList.empty())
		return;

	auto tail = snakeList.end() - 1;

	if (!digestingAppleQ.empty())
	{
		Vector2<int> apple = digestingAppleQ.front();
		if (tail->x == apple.x && tail->y == apple.y) {
			appendSnakeBody(apple.x, apple.y);

			digestingAppleQ.pop();
		}

	}
}

bool GameScreen::updateScore()
{
	score++ ;
	scoreText2.setString(to_string(score));
    return true;
}
void GameScreen::gameOver()
{
    scoreText3.setString(to_string(score));
    highscores.push_back(score);
}

