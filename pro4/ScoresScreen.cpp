#include <SFML/Graphics.hpp>
#include "MainMenuScreen.h"
#include "ScoresScreen.h"
#include "ariblkfont.h"
#include "SnakeGame.h"

const std::string ScoresScreen::ID ="ScoresScreen";

ScoresScreen::ScoresScreen(SnakeGame *game, ScreenManager* manager, GameScreen* games)
{
	this->game = game;
	this->manager = manager;
	this->games = games;

    try{
	if(!texture.loadFromFile("optionsbg.jpg"))
        throw "Loading texture failed";
    sprite.setTexture(texture);
	}catch(const char*exceptfont)
	{
	    std::cout<<"Exception occured"<<exceptfont;
	}

    try{

	if (!font.loadFromMemory(&ariblkfont, ariblkfont_len))
		throw "Loading font failed";
    }
    catch(const char*exceptfont)
    {
        std::cout<<"Exception occured "<<exceptfont;
    }


		textFront.setFont(font);
		textFront.setString("LAST 5 SCORES");
		textFront.setCharacterSize(70);
		textFront.setFillColor(Color::White);
		textFront.setPosition(30, 80);


		textQuit.setFont(font);
		textQuit.setString("Q-quit");
		textQuit.setCharacterSize(50);
		textQuit.setFillColor(Color::White);
		textQuit.setPosition(400, 500);

		textScores.setFont(font);
		textScores.setString("1.\t\tpoints\n2.\t\tpoints\n3,\t\tpoints\n4.\t\tpoints\n5.\t\tpoints  ");
		textScores.setCharacterSize(40);
		textScores.setFillColor(Color::White);
		textScores.setPosition(50, 200);

        for( int i=0;i<5;++i)
        {
		Score[i].setFont(font);
		Score[i].setString("L");
		Score[i].setCharacterSize(40);
		Score[i].setFillColor(Color::Green);
		Score[i].setPosition(120, 200+i*47);
        }

}

void ScoresScreen::Start()
{

}

void ScoresScreen::Update()
{

}

void ScoresScreen::InputUpdate(Event &e)
{

	if (e.type == Event::KeyPressed) {
		if (e.key.code == Keyboard::Key::Q || e.key.code == Keyboard::Key::L ) {
			manager->switchScene(MainMenuScreen::ID);
		}

	}
}

void ScoresScreen::Draw(RenderWindow& window)
{
    window.draw(sprite);
	window.draw(textFront);
	window.draw(textScores);
	window.draw(textQuit);
	unsigned int i=games->highscores.size();
	while(i!=0)
    {
        Score[i-1].setString(to_string(games->highscores[i-1]));
        window.draw(Score[i-1]);
        --i;
    }


}
