#pragma once
#ifndef _RECT_H
#define _RECT_H

#include <SFML/Graphics.hpp>
#include <iostream>

using namespace sf;

class Rectangle
{
public:
	Rectangle(); //konstruktor  klasy Rect
	~Rectangle(); //destruktor klasy Rect

	void draw(RenderWindow* renderer); //funkcja wyrysowujaca obiekty w wyrenderowanym oknie
	void setPosition(float x, float y); //funkcja ustawiajaca wspolrzedne jednego prostokata
	Vector2<float>* getPosition(); //funkcja zwracajaca wspolrzedne jednego prostokata

	bool isActive; //funkcja sprawdzajaca czy dany obiekt jest aktywny, jesli tak to prostokat z nim zmienia

	void setWidth(int width);
	void setActiveColor(Color color); // funkcja ustawiajaca aktywny kolor

private:
	Vector2<float> position; //wektor przechowujacy wspolrzedne
	Vector2<float> size; //wektor przechowujacy rozmiar
	RectangleShape *rect;
	Color activeColor; //zmienna typu sf::Color przechowujaca aktualny kolor
};

#endif // !_TILE_H



